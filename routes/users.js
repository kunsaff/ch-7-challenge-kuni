var express = require('express');
var router = express.Router();
// const {user_game, user_game_biodata, user_game_history} = require ('../models')
const {addUser, viewDelUser, viewEditUser, editUser} = require ('../controllers/usersController')

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource *dari template engine');
});

//keterangan: link: localhost:999/user/.....
//CRUD by admin
router.post('/', addUser)
router.get('/:id/delete', viewDelUser)
router.get('/:id/edit', viewEditUser)
router.post('/:id/update', editUser)

module.exports = router;
