var express = require('express');
var router = express.Router();
// const {user_game, user_game_biodata, user_game_history} = require ('../models')
const {viewHome, viewLogin, viewSignup, viewContact, viewAboutus, viewWork, viewGamesuit, viewDashboard, 
  login, signup} = require ('../controllers/indexController')
const restrictLocal = require('../middlewares/restrict-local')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/home', viewHome)
router.get('/login', viewLogin)
router.get('/signup', viewSignup)
router.get('/contact', viewContact)
router.get('/work', viewWork)
router.get('/about-us', viewAboutus)
router.get('/gamesuit', restrictLocal, viewGamesuit)
router.get('/dashboard', restrictLocal, viewDashboard)

router.post('/login', login)

router.post("/signup", signup)

module.exports = router;
