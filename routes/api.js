//maaf mas, sudah coba otak-atik untuk controllers api & middleware JWT tapi tadi error, token ter-generate tp tdk bs dibaca headernya (token not found)
const router = require('express').Router()
const {user_game} =  require('../models')
const passportJWT = require('../lib/passport-jwt')
const {apiLogin, apiHome, apiMe, createRoom, joinRoom} = require('../controllers/apiController')


const restrictJWT = passportJWT.authenticate('jwt', {
    session: false
})

router.post('/login', apiLogin)

router.get('/home', restrictJWT, apiHome)

router.get('/me', restrictJWT, apiMe)

router.post('/create-room', restrictJWT, createRoom)

router.post('/join', restrictJWT, joinRoom)

router.post('/room_id', restrictJWT, (req, res) => {
    const {username} = req.user.dataValues
    res.send(`${username} sudah masuk ke room`)
})

module.exports = router