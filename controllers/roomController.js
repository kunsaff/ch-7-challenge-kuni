const {user_game, user_game_biodata, user_game_history} = require ('../models')

module.exports = {
    viewRoom: (req, res) => {
        user_game.findAll({
          include: user_game_biodata
        })
        .then(users => {
          res.render("room", {
            users})
        })
    }
}