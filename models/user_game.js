'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.user_game_biodata, {foreignKey:"user_id"}),
      this.hasOne(models.user_game_history, {foreignKey:"user_id"})
    }
    
    //method untuk melakukan enkripsi
    static #encrypt = (password) => bcrypt.hashSync(password, 10)

    // Lalu, kita buat method signup
    static signup = ({ username, password }) => {
      const encryptedPassword = this.#encrypt(password)

      return this.create({username, password: encryptedPassword, isSuperAdmin: false})
    }

    //coba buat method untuk create username by admin (berhasil)
     static addUser = ({ username, password }) => {
      const encryptedPassword = this.#encrypt(password)

      return this.create({username, password: encryptedPassword, isSuperAdmin: false})
    }

    checkPassword = password => bcrypt.compareSync(password, this.password)
    //* Method Authenticate, untuk login *
    
    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.username
      }
      const rahasia = 'ini rahasia ga boleh  disebar-sebar'
      const token = jwt.sign(payload, rahasia)
      return token
    }

    static authenticate = async ({ username, password }) => {
      try {
      const user = await this.findOne({ where: { username }})
      if (!user) return Promise.reject("User not found!")
      console.log('username found')
      const isPasswordValid = user.checkPassword(password)
      if (!isPasswordValid) return Promise.reject("Wrong password")
      console.log('password found')
      return Promise.resolve(user)
      }
      catch(error) {
      return Promise.reject(error)
      }
      
     
  }


  }

  user_game.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    isSuperAdmin: {
      defaultValue: false,
      type: DataTypes.BOOLEAN
    }
  }, {
    sequelize,
    modelName: 'user_game',
  });
  return user_game;
};